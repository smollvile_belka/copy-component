﻿using UnityEditor;
using UnityEngine;

public class CopyComponent: EditorWindow
{
    private static GameObject objects;
    private static Component[] parentComponents;
    private static Component[] childComponents;


    private static bool isParent = false;
    private static bool isChild = false;

    private static bool isChildCount = false;
    
    [MenuItem("Copy Components/Copy Component/Copy Component")]
    public static void Process()
    {
        if(Selection.gameObjects.Length == 0) Debug.Log($"<color=red><b><size=18>Выделите игровой объект!</size></b></color>");
        else if(Selection.gameObjects.Length > 1) Debug.Log($"<color=red><b><size=18>Выделите один объект!</size></b></color>");
        else
        {
            objects = Selection.gameObjects[0];
            
            parentComponents = objects.transform.GetComponents(typeof(Component));
            
            if(objects.transform.childCount != 0)
            {
                isChildCount = true;
                childComponents = objects.transform.GetChild(0).GetComponents(typeof(Component));
            }

            EditorWindow.GetWindow(typeof(CopyComponent));
        }

    }

    void OnGUI () 
    {

        isParent = EditorGUILayout.Toggle("Parent components", isParent);

        if(isChildCount) 
            isChild = EditorGUILayout.Toggle("Child components", isChild);

        if(isParent)
        {
            for(int i = 0; i < parentComponents.Length; i++)
            {
                EditorGUILayout.HelpBox(EditorUtility.InstanceIDToObject(parentComponents[i].GetInstanceID()).ToString(), MessageType.None);
            }
        }  

        if(isChild)
        {
            for(int i = 0; i < childComponents.Length; i++)
            {
                EditorGUILayout.HelpBox(EditorUtility.InstanceIDToObject(childComponents[i].GetInstanceID()).ToString(), MessageType.None);
            }
        }  

        if (GUILayout.Button("Save"))
            this.Close();
    }


    [MenuItem("Copy Components/Copy Component/Add Component")]
    public static void Proces()
    {

        for( int i = 0; i < Selection.gameObjects.Length; i++)
        {

            if(isParent)
            {
                for(int y = 0; y < parentComponents.Length; y++)
                {
                    if(!Selection.gameObjects[i].GetComponent(parentComponents[y].GetType()))
                        ObjectFactory.AddComponent(Selection.gameObjects[i], parentComponents[y].GetType());
                }
            }
    
            if(isChild && isChildCount)
            {
                for(int y = 0; y < childComponents.Length; y++)
                {
                    if(!Selection.gameObjects[i].transform.GetChild(0).GetComponent(childComponents[y].GetType()))
                        ObjectFactory.AddComponent(Selection.gameObjects[i].transform.GetChild(0).gameObject, childComponents[y].GetType());
                }
            }
        }

    }
}
